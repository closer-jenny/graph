import urllib.request, json 
from pandas.io.json import json_normalize
import matplotlib.pyplot as plt
import pandas as pd

with urllib.request.urlopen("https://raw.githubusercontent.com/CLOSER-Cohorts/Study-Overview-Data/master/src/ALSPAC/lifestage.json") as url_lifestage:
    data_lifestage = json.loads(url_lifestage.read().decode())

df_lifestage = json_normalize(data_lifestage)
print(df_lifestage)

plt.figure()
plt.bar(df_lifestage['Lifestage'], df_lifestage['variables'])
plt.savefig('Lifestage_bar.png')


with urllib.request.urlopen("https://raw.githubusercontent.com/CLOSER-Cohorts/Study-Overview-Data/master/src/ALSPAC/topics.json") as url_topics:
    data_topics = json.loads(url_topics.read().decode())
    print(data_topics)

df_topics = json_normalize(data_topics)
df_topics = df_topics.loc[(df_topics.Topic != 'Expectations') , :]

plt.figure()
plt.bar(df_topics['Topic'], df_topics['variables'])
plt.savefig('Topic_bar.png')


